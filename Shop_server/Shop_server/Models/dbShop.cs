﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Shop_server.Models
{
    public class dbShop:DbContext
    {
        public dbShop(DbContextOptions<dbShop> db):base(db)
        {

        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }
    }
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ParentId { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [ForeignKey("Category")]
        public int CatID { get; set; }
        public string ImagePath { get; set; }
        public float Price { get; set; }
        public Category Category { get; set; }
        public ICollection<OrderDetails> OrderDetails { get; set; }

    }
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string ShippingAddress { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
    public class Order
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]

        public DateTime Orderdate { get; set; }
        [ForeignKey("Customer")]
        public int CustId { get; set; }
        public int TotalQty { get; set; }
        public float TotalPrice { get; set; }
        public string Paymethod { get; set; }
        public string OrderNo { get; set; }
        public Customer Customer { get; set; }
        public ICollection<OrderDetails> OrderDetails { get; set; }
    }
    public class OrderDetails
    {
        public int Id { get; set; }
        [ForeignKey("Order")]
        public int OrderID { get; set; }
        [ForeignKey("Product")]
        public int PrdID { get; set; }
        public int Qty { get; set; }

        public int Price { get; set; }

        public Order Order { get; set; }
        public Product Product { get; set; }

    }
}
