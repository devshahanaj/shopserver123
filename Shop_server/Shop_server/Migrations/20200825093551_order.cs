﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop_server.Migrations
{
    public partial class order : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OrderNo",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Paymethod",
                table: "Orders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderNo",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Paymethod",
                table: "Orders");
        }
    }
}
