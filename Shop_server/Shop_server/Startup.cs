﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shop_server.Models;

namespace Shop_server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            //services.AddControllers()
            //    .AddNewtonsoftJson(options => {
                    
            //        options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            //    });

            services.AddDbContext<dbShop>(op=> {
                op.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddCors(o =>
            {
                o.AddPolicy("shop", s =>
                {
                    s.AllowAnyHeader();
                    s.AllowAnyMethod();
                    s.AllowAnyOrigin();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors("shop");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
